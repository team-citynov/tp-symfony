<?php

namespace AppBundle\Service;
use Symfony\Component\HttpFoundation\Request;


class Encryption extends \Twig_Extension
{
    /**
     * Clé de cryptage unique et secrète
     * @var string
     */
    private $_salt = "helloworld";

    /**
     * Méthode de cryptage utilisée
     * @var string
     */
    private $_method = "AES-256-CBC";

    /**
     * Générateur de Token CSFR fourni par Symfony
     */
    private $_csfr_provider;

    /**
     * Constructeur, permet l'injection du CSFR Provider
     * @param type $csfrprovider
     */
    public function __construct($csfrprovider){
        $this->_csfr_provider = $csfrprovider;
    }

    /**
     * Encrypte une chaîne de caractères
     *
     * @param string $stringToEncrypt
     * @return string
     */
    public function encrypt($stringToEncrypt){
//        TODO
        return $stringToEncrypt;
    }

    /**
     * Decrypte une chaîne de caractères
     *
     * @param string $stringToDecrypt
     * @return string
     */
    public function decrypt($stringToDecrypt){
//        TODO
        return $stringToDecrypt;
    }

    /**
     * \Authentifie un CSRF Token dans une Requete
     *
     * ATTENTION : le CSRF Token doit être présent dans le body de la Requête de type POST, dans le champ "token"
     * @param Request $request
     * @return boolean
     */
    public function authenticate(Request $request){
        if($request->request->has('token')){
            $hash = $request->request->get('token');
        }else{
            return false;
        }

        if ($this->_csfr_provider->isCsrfTokenValid('authenticate', $this->decrypt($hash))) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * Twig support
     * @return string
     */
    public function getName() {
        return 'Encryption';
    }

    /**
     * Twig support
     * @return array
     */
    public function getFunctions() {
        return array(
            'encrypt' => new \Twig_Function_Method($this, 'encrypt'),
            'decrypt' => new \Twig_Function_Method($this,'decrypt'),
        );
    }
}