<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface as FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager as ObjectManager;
use AppBundle\Entity\Categorie as Categorie;
use AppBundle\Entity\Produit as Produit;

class InitialData implements FixtureInterface{

    public function load(ObjectManager $manager)
    {
        $categories = array();

        $categories['consommable'] = new Categorie();
        $categories['consommable']->setLibelle('Consommables');
        $categories['consommable']->setReference('0000001');

        $categories['informatique'] = new Categorie();
        $categories['informatique']->setLibelle('Informatique');
        $categories['informatique']->setReference('0000002');

        $categories['cartes_meres'] = new Categorie();
        $categories['cartes_meres']->setLibelle('Cartes Mères');
        $categories['cartes_meres']->setReference('0000003');

        $categories['cartes_graphiques'] = new Categorie();
        $categories['cartes_graphiques']->setLibelle('Carte Graphique');
        $categories['cartes_graphiques']->setReference('0000004');

        $categories['service'] = new Categorie();
        $categories['service']->setLibelle('Services');
        $categories['service']->setReference('0000005');

        $categories['informatique']->addSousCategories([
            $categories['cartes_meres'],
            $categories['cartes_graphiques']
        ]);

        $produits = array();

        $produits['aerosol'] = new Produit();
        $produits['aerosol']->setLibelle('Dacomex bombe dépoussiérante à air comprimé (500 g)');
        $produits['aerosol']->setReference('PB00041215');
        $produits['aerosol']->setReferenceFournisseur('3569875');
        $produits['aerosol']->setDescription('Le nettoyage du matériel informatique et tout particulièrement des composants est une chose délicate. C\'est pour cela qu\'il existe des solutions de nettoyage à air sec, afin d\'éviter à l\'humidité de détériorer irrémédiablement votre matériel.');
        $produits['aerosol']->setType('consommable');

        $produits['chiffon'] = new Produit();
        $produits['chiffon']->setLibelle('Tissu de nettoyage doux');
        $produits['chiffon']->setReference('PB00111229');
        $produits['chiffon']->setReferenceFournisseur('5548796');
        $produits['chiffon']->setDescription('Ce tissu de nettoyage en microfibre est particulièrement adapté au nettoyage d\'un écran, mais pourra être utilisé pour de nombreuses autres applications.');
        $produits['chiffon']->setType(1);

        $produits['carte_asus'] = new Produit();
        $produits['carte_asus']->setLibelle('ASUS GeForce GTX 1080 ROG STRIX-GTX1080-A8G-GAMING');
        $produits['carte_asus']->setReference('PB00212522');
        $produits['carte_asus']->setReferenceFournisseur('2456893');
        $produits['carte_asus']->setDescription('Avec la carte graphique ASUS ROG STRIX-GTX1080-A8G-GAMING, le jeu devient enfin REALITE : Surpuissante et prête à vous propulser dans l\'unvivers extraordinaire de la Réalité Virtuelle, la nouvelle bombe de NVIDIA délivre des performances à couper le souffle.');
        $produits['carte_asus']->setType(0);

        $produits['carte_msi'] = new Produit();
        $produits['carte_msi']->setLibelle('MSI GeForce GTX 1080 GAMING X 8G');
        $produits['carte_msi']->setReference('PB00209764');
        $produits['carte_msi']->setReferenceFournisseur('3165478');
        $produits['carte_msi']->setDescription('Surpuissante et discrète grâce à la technologie MSI TORX 2.0 FAN, la carte graphique MSI GeForce GTX 1080 GAMING X 8G est idéale pour les joueurs extrêmes à la recherche d\'un produit efficace, silencieux et ultra-performant.');
        $produits['carte_msi']->setType('composant');

        $produits['raspberry'] = new Produit();
        $produits['raspberry']->setLibelle('Raspberry Pi 2 Model B');
        $produits['raspberry']->setReference('PB00182827');
        $produits['raspberry']->setReferenceFournisseur('6594683');
        $produits['raspberry']->setDescription('Plus puissante et plus rapide que sa petite soeur, Raspberry Pi 2 Type B est la carte mère idéale pour votre système d\'architecture ARM. Elle intègre un puissant processeur Quad-Core ARM Cortex-A7 900 MHz (Broadcom BCM2836), 1024 Mo de RAM et un GPU Dual-Core VideoCore IV.');
        $produits['raspberry']->setType('composant');

        $produits['reparation'] = new Produit();
        $produits['reparation']->setLibelle('Forfait diagnostic et réparation');
        $produits['reparation']->setReference('REPA00001');
        $produits['reparation']->setReferenceFournisseur('1235649');
        $produits['reparation']->setDescription('Diagnostic et réparation tout matériel');
        $produits['reparation']->setType('service');

        $categories['consommable']->addProduits([
            $produits['aerosol'],
            $produits['chiffon']
        ]);

        $categories['cartes_graphiques']->addProduits([
            $produits['carte_asus'],
            $produits['carte_msi']
        ]);

        $categories['cartes_meres']->addProduits([
            $produits['raspberry']
        ]);

        $categories['service']->addProduit($produits['reparation']);

        foreach($categories as $categorie) {
            $manager->persist($categorie);
        }

        foreach($produits as $produit){
            $manager->persist($produit);
        }
        $manager->flush();
    }
}