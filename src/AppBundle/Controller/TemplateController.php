<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TemplateController extends Controller{

    /**
     * @param Request $request
     * @param $name
     * @return mixed
     */
    public function showAction(Request $request, $name){
        return $this->render($name.'.html.twig');
    }
}